import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Visor de Imagenes',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Visor de Imagenes'),
      debugShowCheckedModeBanner: false,
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final controlador = TextEditingController();
  String texto = "images/android";

  @override
  void dispose() {
    controlador.dispose();
    super.dispose();
  }

  void _showDialogImage() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text("Visualizacion"),
          content: Image.asset(texto + '.jpg'),
          actions: <Widget>[
            FlatButton(
                onPressed: () {
                  Navigator.of(context).popUntil((route) => route.isFirst);
                },
                child: new Text("Cerrar")),
          ],
        );
      },
    );
  }

  void _showDialog() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text("Abrir imagen"),
          content: TextFormField(
            controller: controlador,
            decoration: InputDecoration(
              labelText: "Ingrese ruta de la imagen",
            ),
          ),
          actions: <Widget>[
            FlatButton(
              onPressed: () {
                _recuperarTexto(controlador.text);
                _showDialogImage();
              },
              child: new Text("Ver Imagen"),
            ),
            FlatButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: new Text("Cerrar")),
          ],
        );
      },
    );
  }

  void _recuperarTexto(String text) {
    setState(() {
      texto = text;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: FlatButton(
          onPressed: () {
            _showDialog();
          },
          child: Text('Abrir'),
          color: Colors.blue,
          textColor: Colors.white,
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.exit_to_app),
        onPressed: () => SystemNavigator.pop(),
      ),
    );
  }
}
